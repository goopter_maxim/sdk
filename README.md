# star-printer-sdk

[![CI Status](https://img.shields.io/travis/reena@goopter.com/star-printer-sdk.svg?style=flat)](https://travis-ci.org/reena@goopter.com/star-printer-sdk)
[![Version](https://img.shields.io/cocoapods/v/star-printer-sdk.svg?style=flat)](https://cocoapods.org/pods/star-printer-sdk)
[![License](https://img.shields.io/cocoapods/l/star-printer-sdk.svg?style=flat)](https://cocoapods.org/pods/star-printer-sdk)
[![Platform](https://img.shields.io/cocoapods/p/star-printer-sdk.svg?style=flat)](https://cocoapods.org/pods/star-printer-sdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

star-printer-sdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'star-printer-sdk'
```

## Author

reena@goopter.com, maxim@goopter.com

## License

star-printer-sdk is available under the MIT license. See the LICENSE file for more info.
